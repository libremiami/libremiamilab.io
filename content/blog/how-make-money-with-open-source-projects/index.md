---
title: How Developers Can Make money with Open Source 
date: "2020-02-13"
description: "How to make money with open source. How to make profits as an open source developer. Businesses and companies that make money in open source."
featuredImage: "./money.jpg"
photoCredit: "Alexander Mils, Unsplash License"
---
When people think of open source software, they often imagine volunteer work and software they can get for free. Contrary to what many believe, working with free/libre open source software doesn’t mean you can’t make a living as a software developer or have a profitable software business.

In fact, in the early days of open source, plenty of people made money selling tape and CD copies of programs. Since then, new business models have been created to adapt to changing times. Here are the latest proven ways to make profits from open source software.

## Develop or Customize Software for a Service, Software as a Service (OpenSaaS)

If you can provide a service using software people will pay for the service. There are different ways to do this well, let’s look at a few case studies.

### Wordpress

[Wordpress](https://wordpress.com/) runs 35% of the internet. Wordpress is both a web hosting service, and the content management system customers use to manage their websites, released under GPLv2+. People are confident in using Wordpress because of its popularity, due in no small part to its open source ecosystem, and because there is no vendor lock-in.

### Canvas

[Canvas](https://www.instructure.com/canvas/portfolium), similar to wordpress, is both a learning management system released under AGPLv3, and a hosted learning management system solution.
 
## Develop or Customize Software for a Hardware Product

If you’re developing software for a hardware product, you can add value by making it open source. That way consumers don’t have to worry about your product becoming an expensive paper weight if you decide to stop supporting it. They can also better tailor the hardware to their needs. You could just distribute the source code and a suitable license along with the product, so that generally only people who buy the product get the software. On the other hand, some companies leverage open contribution models by publishing the source online.

### Purism

[Purism](puri.sm) sells laptops and smartphones. They have developed a number of software projects for their hardware products, including PureOS (a fully libre operating system).

## Trademark Licensing

You can license your trademark to service providers and charge them a fee.

### Moodle

[Moodle](https://moodle.com/) is another learning management system, like canvas. They make some of their money by licensing their trademark to service providers who then have permission to market their Moodle service.

## Recurrent Crowd-funding, Recurrent Donations, Patronage, Recurring Pledges

Websites like patreon and [Liberapay](https://en.liberapay.com/) have showed that people will pay a monthly membership to support projects they care about, especially when there are incentives.

### Godot

[Godot](https://godotengine.org/) is a free and open source game engine that receives close to $11,000 in monthly patreon support at the time of writing. In addition to making the continued development of the software possible, patrons get different levels of esteem, access to the developers, and say in the direction of the software. 

### Vue

[Vue](https://vuejs.org/) has a patreon that recieves over $18,000 a month at the time of writing. Vue also has contributors on [Open Collective](https://opencollective.com/vuejs), where the project recieves another 5-figure sum

## Sponsorship

In addition to recurrent payments from individual users, sponsors can make payments to leverage the attention of users. Not only is sponsoring open source projects great for public relations, sometimes it can be tax-deductible and serve the interests of the sponsoring organization at the same time. Projects will advertize sponsors on their website, documentation, and sometimes in the software itself. 

### Godot

In addition to individual patrons, Godot recieves money from a number of game companies.

### Vue

In addition to individual pledges, Vue also gets money from its sponsors. These include developer service providers, coding education providers, software consultancies, digital marketing agencies, technology conferences and more. Vue makes a great case being a sponsor:

>If you run a business and are using Vue in a revenue-generating product, it makes business sense to sponsor Vue development: it ensures the project that your product relies on stays healthy and actively maintained. It can also help your exposure in the Vue community and makes it easier to attract Vue developers. 

## Selling Support, Certifications and Training

Enterprise users will often need some guarantee that their mission-critical software will continue running and validation that their employees undergo the continuing education needed to keep pace with competitors. Paying certified support specialists can often be cheaper than having in-house specialists on payroll. 

### RedHat

Rich Bynum, a member of Red Hat's legal team, attributes Linux's success and rapid development partially to open-source business models, including Red Hat's. Red hat has made enough money to support and contribute to a number of open source projects, including One Laptop per Child, GNOME, and LibreOffice. They were recently acquired by IBM.

## Pay-what-you-want-distribution, Subscriptions

Just because your software is open source, doesn't mean you need to distribute it for free. There is value in being able to download an official binary.

### Ardour

[Ardour](http://ardour.org/) is a digital audio workstation that currently distributes binaries at a suggested price of $45 dollars, and has monthly subscriptions starting at $1.

## Funding from Business Leagues, 501(c)6 non-profit organizations

Some open source projects recieve funding from non-profit organizations that represent a class of business.

### Linux Foundation

The Linux foundation not only supports the Linux kernel, but also dozens of other open source projects like Grunt and Node.js, and a number of other organizations like Alliance for Open Media and OpenJS foundation. Its membership includes household names like AT&T, Cisco, Google, Intel, IBM and even Microsoft.

## Conclusion

Hopefully you are convinced by now. Free/libre open source software doesn't necessarily mean non-commercial software. There is plenty of commercial open source software. As time passes, further innovation in open source business models will continue to move hand-in-hand with further innovations in technology.
