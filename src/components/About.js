import React from 'react'
import './About.css'
import Profile from "./Profile"
import { Link, graphql, useStaticQuery } from "gatsby"

export default function About() {
    const data = useStaticQuery(graphql`
        query {
            members: allMembersJson {
                nodes {
                    title
                    handle
                    description
                    picture
                    website
                }
            }
        }
    `);
    const members = data.members.nodes;
    return (
        <section id="about" class="about-section container">
            <div class="about-text-container">
                <div>
                    <h2>Our Mission</h2>
                    <p>We believe you should be free to use your software as you wish, adapt it to your needs (maybe with some help), and spread it through your community. We promote libre software like LibreOffice, Firefox and GNU/Linux which respects your freedom over proprietary software like Microsoft Office, Chrome, and macOS which restricts users.</p>
                    <p>LibreMiami is here to build a future where everyone can be their best selves without proprietary software, by sharing libre knowledge, advocating for user freedom, and advancing the state of libre software. </p>
                    <h3>Okay, but what do you do specifically?</h3>
                    <p>We help people meet their software and hardware needs. We help people learn to code. We help people contribute to open source. We fearlessly tackle the big issues. We work on what we really care about. We hang out in pink rooms, drink boba tea and laugh at our commit messages.</p>
                </div>
                <div id="join">
                    <h2>Thank you!</h2>
                    <p>LibreMiami is currently inactive. A sincere thanks to everyone who took time to support this vision. You might be interested in the following groups instead:</p>
                    <ol>
                        <li>@jgart is running a <a href="https://whereiseveryone.srht.site/">Guix group</a>.</li>
                        <li><a href="https://cyberia.club/">Cyberia Computer Club</a> is a cool Minnesota group that is pretty libre-friendly</li>
                    </ol>
                    <p>Stay tuned! Happy Hacking.</p>
                </div>
            </div>
            <h2 style={{marginTop: `1em`, textAlign: `center`}}>Contributors</h2>
            <div className="profiles">
                { members.map(member => <Profile title={member.title} handle={member.handle} description={member.description} picture={member.picture} website={member.website} />) }
            </div>
        </section>
    )
}
