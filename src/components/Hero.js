import React from 'react'
import './Hero.css'

export default function Hero() {
    return (
        <section id="home" class="hero-section container">
            <h1>Libre<span class="heavy">Miami</span></h1>
            <p class="hero-subheading">Advocating libre and open source software for Miami (and beyond)</p>
        </section>
    )
}
