import React from 'react';
import NavBar from './NavBar';
import Hero from './Hero';
import About from './About';
import Events from './Events';
import Blog from './Blog';

function App() {
  return (
    <>
      <NavBar />
      <main>
        <Hero />
        <About />
        <Blog />
      </main>
    </>
  );
}

export default App;
