import React from 'react'

export default function Meetup(props) {
    return (
        <div>
            <h3>{props.title}</h3>
            <p>
                {props.description}
            </p>
            <p>
                {props.frequency} @ <a href={props.locationLink}>{props.location}</a>.
            </p>
            <p>
                {props.address1 && <>{props.address1} <br/></>}
                {props.address2 && <>{props.address2} <br/></>}
                {props.startTime} &ndash; {props.endTime}
            </p>
            <p>
                <a href={props.pageLink} target="_blank" rel="noopener">More info</a>
            </p>
            {
                props.channels && <> 
                    <h4>Channels</h4>
                    <ul>
                    {Object.entries(props.channels).map(([key, value]) =>
                        <li><a href={value} target="_blank" rel="noopener">{key}</a></li>
                    )}
                    </ul>
                </>
            }
        </div>
    )
}