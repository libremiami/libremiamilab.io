import React from 'react'
import "./ButtonLink.css"

export default function ButtonLink({link, text}) {
    return (
        <div>
            <a href={link} class="btn-link">{text}</a>
        </div>
    )
}
