import React from 'react'
import Meetup from "./Meetup.js"
import "./Events.css"

export default function Events() {
    return (
        <section id="events" class="events-section container">
            <div id="meetups">
                <h2>Regular Meetups</h2>
                <p>To keep everyone safe from COVID-19, we&rsquo;re only doing online meetups for now. Check <a href="https://riot.im/app/#/room/!bjpDRCNuFsHwtGwgsA:matrix.org?via=matrix.org" target="_blank" rel="noopener">the chat</a> for the latest.</p>
                <div className="meetups-container">
                    {/* 
                        Meetup parameters 
                        - Title: what the name of the meetup is
                        - description: what the meetup is about. Keep short, like ~100 characters or one/two sentences)
                        - frequency: how often. Day of the month, week, etc.
                        - location: where it is happening, online or not
                        - locationLink: link to the online location or information on the physical location
                        - address1: street and apt/suite
                        - address2: city, state, zip
                        - startTime: when the meetup starts
                        - endTime: when the meetup ends
                        - pageLink: link to the event page for the next meetup
                        - channels: links to communication channels related to the meetup
                    */}
                    <Meetup
                        title="Guix Packaging"
                        description="Packaging for Guix. Open to all skill levels"
                        frequency="Last Saturday of every month"
                        location="LibreMiami Mumble"
                        locationLink="https://mumble.libremiami.org"
                        startTime="2pm"
                        endTime="4pm"
                        pageLink="https://events.nixnet.services/events/27955ca1-0aee-4ec5-be20-48e6c45fd0f6"
                        channels={
                            {
                                "Matrix": "https://riot.im/app/#/room/#guix-packaging:matrix.org",
                                "IRC": "https://webchat.freenode.net/#libremiami-guixpackagers"
                            }
                        }
                    />
                </div>
            </div>
        </section>
    )
}
